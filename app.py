import logging
import json
import os
import datetime
import time

from flask import Flask, Response, request, jsonify

from google.appengine.ext import ndb

app = Flask(__name__, static_url_path='', static_folder='app')
app.add_url_rule('/', 'root', lambda: app.send_static_file('index.html'))


class Epic(ndb.Model):
	author = ndb.StringProperty()
	content = ndb.StringProperty()
	order = ndb.IntegerProperty()
	title = ndb.StringProperty()


@app.route('/api/posts', methods=['GET', 'POST'])
def get_posts():

	query = Epic.query()
	#query.filter('author =', 'Paul Barnett')
	query.order = ['order']
	results = query.fetch()	

	json_output = json.dumps([dict(p.to_dict(), **dict(id=p.key.id())) for p in results])

	return Response(json_output, mimetype='application/json')


@app.route('/api/posts/order', methods=['GET', 'POST'])
def update_order():
	length = request.json['length']

	logging.warning(length)
	json = request.json

	i = 0
	while (i < length):
		epic_id = json[str(i)]['id']
		order = json[str(i)]['order']
		epic = Epic.get_by_id( epic_id )
		epic.order = order
		epic.put()
		logging.warning( epic )
		i = i + 1

	json_output = ''
	return Response( json_output, mimetype='application/json')


@app.route('/api/posts/add', methods=['GET', 'POST'])
def add_post():
	title = request.json['title']
	content = request.json['content']
	order = int(request.json['order'])
	author = request.json['author']

	logging.warning(title)

	epic = Epic(author=author,
							content = content,
							order = order,
							title = title)

	logging.warning(epic)

	epic.put()

	json_output = json.dumps( dict( epic.to_dict() ) )

	return Response( json_output, mimetype='application/json')


@app.route('/api/posts/update', methods=['GET', 'POST'])
def update_post():	
	epic_id = request.json['id']
	title = request.json['title']
	content = request.json['content']

	logging.warning(epic_id)
	
	epic = Epic.get_by_id( epic_id )
	epic.title = title
	epic.content = content
	epic.put() 

	logging.warning(epic)

	json_output = json.dumps( dict( epic.to_dict() ) )

	return Response( json_output, mimetype='application/json')


@app.route('/api/posts/delete', methods=['GET', 'POST'])
def delete_post():
	epic_id = request.json['id']
	title = request.json['title']
	content = request.json['content']

	logging.warning(epic_id)

	epic = Epic.get_by_id( epic_id )

	logging.warning(epic)

	json_output = json.dumps( dict( epic.to_dict() ) )

	epic.key.delete()	

	return Response( json_output, mimetype='application/json' )


@app.errorhandler(500)
def server_error(e):
	# Log the error and stacktrace.
	logging.exception('An error occurred during a request.')
	return 'An internal error occurred.', 500
# [END app]