module.exports = function(grunt) {
  var sass = require('node-sass');

  grunt.initConfig({
    themesPath: 'app/',
    sass: {
      options: {
        implementation: sass,
        outputStyle: 'compressed',
        sourceMap: true
      },
      dist: {
        files: {
          '<%= themesPath %>/css/style.css' : '<%= themesPath %>/src/scss/main.scss'
          }
      }
    },

    watch: {
      styles: {
        files: ['<%= themesPath %>/src/scss/*.scss'],
        tasks: ['sass:dist']
      }
    }
  });

  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('build', ['sass:dist']);
  grunt.registerTask('default',['build']);
}
